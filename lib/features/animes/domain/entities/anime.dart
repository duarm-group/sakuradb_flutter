class Anime {
  final int id;
  final String name;
  final String nameTranslated;
  final int episodeCount;
  final String type;
  final String? studio;
  final String? about;
  final String? image;

  const Anime({
    required this.episodeCount,
    required this.type,
    required this.id,
    required this.name,
    required this.nameTranslated,
    this.image,
    this.studio,
    this.about,
  });

  @override
  String toString() {
    return "($id,$name,$nameTranslated,$name,$type,$studio,$image,$about)";
  }
}
