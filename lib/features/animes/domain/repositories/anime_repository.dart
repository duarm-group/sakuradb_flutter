import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../entities/anime.dart';

abstract class AnimeRepository {
  Future<Either<Failure, List<Anime>>> getAnimes();

  Future<Either<Failure, Anime>> getAnime(int id);
}
