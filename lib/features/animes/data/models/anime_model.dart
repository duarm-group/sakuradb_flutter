import '../../domain/entities/anime.dart';

class AnimeModel extends Anime {
  AnimeModel(
      {required super.id,
      required super.name,
      required super.nameTranslated,
      required super.episodeCount,
      required super.type,
      super.about,
      super.studio,
      super.image});

  factory AnimeModel.fromJson(Map<String, dynamic> map) {
    return AnimeModel(
      id: map['id'],
      name: map['name'],
      nameTranslated: map['name_translated'],
      episodeCount: map['episode_count'],
      type: map['type'],
      about: map['about'],
      studio: map['studio'],
      image: map['image'],
    );
  }
}

class AnimeDataModel {
  AnimeDataModel({required this.code, required this.msg, required this.data});

  final int code;
  final String msg;
  final Anime data;

  factory AnimeDataModel.fromJson(Map<String, dynamic> map) {
    return AnimeDataModel(
      code: map['code'],
      msg: map['msg'],
      data: AnimeModel.fromJson(map['data']),
    );
  }
}

class AnimesData {
  AnimesData({required this.lists, required this.total});

  final List<Anime> lists;
  final int total;

  factory AnimesData.fromJson(Map<String, dynamic> map) {
    return AnimesData(
      lists:
          List.from(map['lists']).map((e) => AnimeModel.fromJson(e)).toList(),
      total: map['total'],
    );
  }
}

class AnimesModel {
  AnimesModel({required this.code, required this.msg, required this.data});

  final int code;
  final String msg;
  final AnimesData data;

  factory AnimesModel.fromJson(Map<String, dynamic> map) {
    return AnimesModel(
      code: map['code'],
      msg: map['msg'],
      data: AnimesData.fromJson(map['data']),
    );
  }
}
