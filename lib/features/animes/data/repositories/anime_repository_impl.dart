import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:sakuradb_app/core/error/exceptions.dart';
import 'package:sakuradb_app/core/error/failure.dart';

import '../../domain/entities/anime.dart';
import '../../domain/repositories/anime_repository.dart';
import '../datasources/anime_remote_data_source.dart';

class AnimeRepositoryImpl implements AnimeRepository {
  AnimeRepositoryImpl({required this.remoteDataSource});

  final AnimeRemoteDataSource remoteDataSource;

  @override
  Future<Either<Failure, Anime>> getAnime(int id) async {
    try {
      final animes = await remoteDataSource.getAnime(id);
      return Right(animes.data);
    } on ServerException {
      return Left(ServerFailure());
    } on SocketException {
      return Left(ServerFailure());
    } catch (e) {
      return Left(OtherFailure());
    }
  }

  @override
  Future<Either<Failure, List<Anime>>> getAnimes() async {
    try {
      final animes = await remoteDataSource.getAnimes();
      return Right(animes.data.lists);
    } on ServerException {
      return Left(ServerFailure());
    } on SocketException {
      return Left(ServerFailure());
    } catch (e) {
      return Left(OtherFailure());
    }
  }
}
