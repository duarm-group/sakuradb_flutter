import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../../core/error/exceptions.dart';
import '../models/anime_model.dart';

abstract class AnimeRemoteDataSource {
  Future<AnimeDataModel> getAnime(int id);

  Future<AnimesModel> getAnimes();
}

class AnimeRemoteDataSourceImpl implements AnimeRemoteDataSource {
  final http.Client client;

  AnimeRemoteDataSourceImpl(this.client);

  @override
  Future<AnimeDataModel> getAnime(int id) async {
    final response = await client.get(
      Uri.parse("http://10.0.2.2:8000/api/v1/animes/$id"),
      headers: {
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 200) {
      return AnimeDataModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }

  @override
  Future<AnimesModel> getAnimes() async {
    final response = await client.get(
      Uri.parse("http://10.0.2.2:8000/api/v1/animes/"),
      headers: {
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 200) {
      return AnimesModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }
}
