import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shimmer/shimmer.dart';

import '../../../../core/error/widgets/error_screen.dart';
import '../../../../core/theme/base_theme.dart';
import '../../../../core/theme/widgets/core_scaffold.dart';
import '../cubits/detail/anime_detail_cubit.dart';
import '../cubits/detail/anime_detail_state.dart';

class AnimeDetailScreen extends StatelessWidget {
  const AnimeDetailScreen({super.key, required this.id});

  final int id;

  @override
  Widget build(BuildContext context) {
    context.read<AnimeDetailCubit>().getAnime(id);
    final BaseTheme theme = context.read<BaseTheme>();

    return CoreScaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(56),
        child: BlocBuilder<AnimeDetailCubit, AnimeDetailState>(
          builder: (context, state) {
            return AppBar(
              title: Text(state.anime.nameTranslated),
            );
          },
        ),
      ),
      body: BlocBuilder<AnimeDetailCubit, AnimeDetailState>(
        builder: (context, state) {
          if (state.status == AnimeDetailScreenStatus.success) {
            return SingleChildScrollView(
              child: Column(children: [
                AspectRatio(
                  aspectRatio: 1.2,
                  child: ShaderMask(
                    shaderCallback: (rect) {
                      return const LinearGradient(
                        begin: FractionalOffset(0, 0.5),
                        end: FractionalOffset(0, 0.9),
                        colors: [Colors.black, Colors.transparent],
                      ).createShader(
                        Rect.fromLTRB(0, 0, rect.width, 350),
                      );
                    },
                    blendMode: BlendMode.dstIn,
                    child: Image.asset(
                      'assets/images/${state.anime.image}',
                      isAntiAlias: true,
                      fit: BoxFit.fitWidth,
                      alignment: const FractionalOffset(0.0, 0.2),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: CoreSpacing.spacing3),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Flexible(
                            child: Text(
                              state.anime.name,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Text(
                        state.anime.nameTranslated,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w400),
                      ),
                      const SizedBox(
                        height: CoreSpacing.spacing2,
                      ),
                      if (state.anime.about != null)
                        Text(
                          state.anime.about!,
                          style: TextStyle(
                            fontSize: 14,
                            color: theme.gray700,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                    ],
                  ),
                ),
              ]),
            );
          } else if (state.status == AnimeDetailScreenStatus.error) {
            return const ErrorScreen(
                title: "Looks like a server error ocurred",
                subtitle: " hoeee~~");
          }
          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: FractionallySizedBox(
                  widthFactor: 1,
                  alignment: Alignment.topCenter,
                  heightFactor: 0.3,
                  child: Shimmer.fromColors(
                    baseColor: theme.gray400.withOpacity(0.4),
                    highlightColor: theme.gray400.withOpacity(0.1),
                    child: Container(
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
              Shimmer.fromColors(
                baseColor: theme.gray400.withOpacity(0.4),
                highlightColor: theme.gray400.withOpacity(0.1),
                child: Container(
                  color: Colors.black,
                ),
              ),
              Shimmer.fromColors(
                baseColor: theme.gray400.withOpacity(0.4),
                highlightColor: theme.gray400.withOpacity(0.1),
                child: Container(
                  color: Colors.black,
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
