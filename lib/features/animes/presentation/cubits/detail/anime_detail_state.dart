import 'package:equatable/equatable.dart';

import '../../../domain/entities/anime.dart';

enum AnimeDetailScreenStatus { initial, success, loading, error }

class AnimeDetailState extends Equatable {
  const AnimeDetailState(
      {this.status = AnimeDetailScreenStatus.initial,
      this.anime = const Anime(
        id: 0,
        nameTranslated: '',
        type: '',
        name: '',
        episodeCount: 0,
      )});

  @override
  List<Object> get props => [status, anime];

  final AnimeDetailScreenStatus status;
  final Anime anime;

  AnimeDetailState copyWith({
    AnimeDetailScreenStatus? status,
    Anime? anime,
  }) {
    return AnimeDetailState(
      status: status ?? this.status,
      anime: anime ?? this.anime,
    );
  }
}
