import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../domain/repositories/anime_repository.dart';
import 'anime_detail_state.dart';

class AnimeDetailCubit extends Cubit<AnimeDetailState> {
  AnimeDetailCubit({required this.animeRepository})
      : super(const AnimeDetailState());
  final AnimeRepository animeRepository;

  void getAnime(int id) async {
    emit(state.copyWith(status: AnimeDetailScreenStatus.loading));
    final response = await animeRepository.getAnime(id);

    response.fold(
      (e) {
        emit(state.copyWith(status: AnimeDetailScreenStatus.error));
      },
      (anime) {
        emit(state.copyWith(
          anime: anime,
          status: AnimeDetailScreenStatus.success,
        ));
      },
    );
  }
}
