import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../domain/repositories/anime_repository.dart';
import 'animes_state.dart';

class AnimesCubit extends Cubit<AnimesState> {
  AnimesCubit({required this.animeRepository}) : super(const AnimesState());
  final AnimeRepository animeRepository;

  void getAnimes() async {
    emit(state.copyWith(status: AnimesScreenStatus.loading));
    final response = await animeRepository.getAnimes();

    response.fold(
      (e) {
        emit(state.copyWith(status: AnimesScreenStatus.error));
      },
      (animes) {
        emit(state.copyWith(
          animes: animes,
          status: AnimesScreenStatus.success,
        ));
      },
    );
  }
}
