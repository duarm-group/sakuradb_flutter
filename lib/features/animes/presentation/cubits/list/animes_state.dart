import 'package:equatable/equatable.dart';

import '../../../domain/entities/anime.dart';

enum AnimesScreenStatus { initial, success, loading, error }

class AnimesState extends Equatable {
  const AnimesState(
      {this.status = AnimesScreenStatus.initial, this.animes = const []});

  @override
  List<Object> get props => [status, animes];

  final AnimesScreenStatus status;
  final List<Anime> animes;

  AnimesState copyWith({
    AnimesScreenStatus? status,
    List<Anime>? animes,
  }) {
    return AnimesState(
      status: status ?? this.status,
      animes: animes ?? this.animes,
    );
  }
}
