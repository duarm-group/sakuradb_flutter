import 'package:get_it/get_it.dart';
import '../data/datasources/anime_remote_data_source.dart';
import '../data/repositories/anime_repository_impl.dart';
import '../domain/repositories/anime_repository.dart';
import '../presentation/cubits/detail/anime_detail_cubit.dart';
import '../presentation/cubits/list/animes_cubit.dart';

GetIt getIt = GetIt.instance;

class AnimeModule {
  static void inject() {
    getIt.registerSingleton<AnimeRemoteDataSource>(
      AnimeRemoteDataSourceImpl(getIt.get()),
    );

    getIt.registerSingleton<AnimeRepository>(
      AnimeRepositoryImpl(remoteDataSource: getIt.get()),
    );

    getIt.registerSingleton<AnimesCubit>(
      AnimesCubit(animeRepository: getIt.get()),
    );

    getIt.registerFactory<AnimeDetailCubit>(
      () => AnimeDetailCubit(animeRepository: getIt.get()),
    );
  }
}
