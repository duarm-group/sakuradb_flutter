class Card {
  final int id;
  final String name;
  final int alignment;
  final String? image;

  const Card({
    required this.id,
    required this.name,
    required this.alignment,
    this.image,
  });

  @override
  String toString() {
    return "($id,$name,$alignment,$image)";
  }
}
