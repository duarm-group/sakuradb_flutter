import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../entities/card.dart';

abstract class CardRepository {
  Future<Either<Failure, List<Card>>> getCards();

  Future<Either<Failure, Card>> getCard(int id);
}
