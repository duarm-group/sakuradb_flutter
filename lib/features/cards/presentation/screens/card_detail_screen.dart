import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shimmer/shimmer.dart';

import '../../../../core/theme/base_theme.dart';
import '../../../../core/theme/widgets/core_scaffold.dart';
import '../cubit/detail/card_detail_cubit.dart';

class CardDetailScreen extends StatelessWidget {
  const CardDetailScreen({super.key, required this.id});

  final int id;

  @override
  Widget build(BuildContext context) {
    context.read<CardDetailCubit>().getCard(id);
    final theme = context.read<BaseTheme>();

    return CoreScaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(56),
        child: BlocBuilder<CardDetailCubit, CardDetailState>(
          builder: (context, state) {
            return AppBar(
              title: Text(state.card.name),
            );
          },
        ),
      ),
      body: BlocBuilder<CardDetailCubit, CardDetailState>(
        builder: (context, state) {
          if (state.status == CardDetailScreenStatus.success) {
            return SingleChildScrollView(
              child: Column(children: [
                AspectRatio(
                  aspectRatio: 1.2,
                  child: ShaderMask(
                    shaderCallback: (rect) {
                      return const LinearGradient(
                        begin: FractionalOffset(0, 0.5),
                        end: FractionalOffset(0, 0.9),
                        colors: [Colors.black, Colors.transparent],
                      ).createShader(
                        Rect.fromLTRB(0, 0, rect.width, 350),
                      );
                    },
                    blendMode: BlendMode.dstIn,
                    child: Image.asset(
                      'assets/images/${state.card.image}',
                      isAntiAlias: true,
                      fit: BoxFit.fitWidth,
                      alignment: const FractionalOffset(0.0, 0.2),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: CoreSpacing.spacing3),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Flexible(
                            child: Text(
                              state.card.name,
                              style: const TextStyle(
                                  fontSize: 18,
                                  overflow: TextOverflow.ellipsis,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                          if (state.card.alignment == 2)
                            Icon(
                              Icons.sunny,
                              color: theme.pink,
                            ),
                          if (state.card.alignment == 1)
                            SvgPicture.asset(
                              "assets/moon.svg",
                              width: 40,
                            ),
                        ],
                      ),
                      const SizedBox(
                        height: CoreSpacing.spacing2,
                      ),
                    ],
                  ),
                ),
              ]),
            );
          }
          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: FractionallySizedBox(
                  widthFactor: 1,
                  alignment: Alignment.topCenter,
                  heightFactor: 0.3,
                  child: Shimmer.fromColors(
                    baseColor: theme.gray400.withOpacity(0.4),
                    highlightColor: theme.gray400.withOpacity(0.1),
                    child: Container(
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
              Shimmer.fromColors(
                baseColor: theme.gray400.withOpacity(0.4),
                highlightColor: theme.gray400.withOpacity(0.1),
                child: Container(
                  color: Colors.black,
                ),
              ),
              Shimmer.fromColors(
                baseColor: theme.gray400.withOpacity(0.4),
                highlightColor: theme.gray400.withOpacity(0.1),
                child: Container(
                  color: Colors.black,
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
