import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:shimmer/shimmer.dart';

import '../../../../core/error/widgets/error_screen.dart';
import '../../../../core/theme/base_theme.dart';
import '../../../../core/theme/widgets/core_scaffold.dart';
import '../../../home/presentation/widgets/home_button_widget.dart';
import '../cubit/list/cards_cubit.dart';

class CardsScreen extends StatelessWidget {
  const CardsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    context.read<CardsCubit>().getCards();
    final theme = context.read<BaseTheme>();

    return CoreScaffold(
      appBar: AppBar(
        title: const Text("Cards"),
        actions: [
          InkWell(
            child: const Icon(
              Icons.filter_list,
              size: 30,
            ),
            onTap: () {},
          ),
          const SizedBox(width: CoreSpacing.spacing3),
        ],
      ),
      body: BlocBuilder<CardsCubit, CardsState>(
        builder: (context, state) {
          if (state.status == CardsScreenStatus.loading) {
            return GridView.builder(
              itemCount: 5,
              itemBuilder: (_, index) => Shimmer.fromColors(
                baseColor: theme.gray400.withOpacity(0.4),
                highlightColor: theme.gray400.withOpacity(0.1),
                child: Container(
                  color: Colors.black,
                ),
              ),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 5.0,
                mainAxisSpacing: 5.0,
                mainAxisExtent: 300,
              ),
            );
          } else if (state.status == CardsScreenStatus.error) {
            return const ErrorScreen(
                title: "Looks like a server error ocurred",
                subtitle: " hoeee~~");
          }
          return GridView.builder(
            itemCount: state.cards.length,
            itemBuilder: (_, index) => HomeButton(
              title: state.cards[index].name,
              subtitle: "state.cards[index].name",
              image: 'assets/images/${state.cards[index].image}',
              onTap: () => context.go(
                '/cards/detail',
                extra: state.cards[index].id,
              ),
            ),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 5.0,
              mainAxisSpacing: 5.0,
              mainAxisExtent: 300,
            ),
          );
        },
      ),
    );
  }
}
