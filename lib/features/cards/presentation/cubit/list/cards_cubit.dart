import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../domain/entities/card.dart';
import '../../../domain/repositories/card_repository.dart';

part 'cards_state.dart';

class CardsCubit extends Cubit<CardsState> {
  CardsCubit({required this.cardRepository}) : super(const CardsState());
  final CardRepository cardRepository;

  void getCards() async {
    emit(state.copyWith(status: CardsScreenStatus.loading));
    final response = await cardRepository.getCards();

    response.fold(
      (e) {
        emit(state.copyWith(status: CardsScreenStatus.error));
      },
      (cards) {
        emit(state.copyWith(
          cards: cards,
          status: CardsScreenStatus.success,
        ));
      },
    );
  }
}
