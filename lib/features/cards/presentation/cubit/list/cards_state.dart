part of 'cards_cubit.dart';

enum CardsScreenStatus { initial, success, loading, error }

class CardsState extends Equatable {
  const CardsState(
      {this.status = CardsScreenStatus.initial, this.cards = const []});

  @override
  List<Object> get props => [status, cards];

  final CardsScreenStatus status;
  final List<Card> cards;

  CardsState copyWith({
    CardsScreenStatus? status,
    List<Card>? cards,
  }) {
    return CardsState(
      status: status ?? this.status,
      cards: cards ?? this.cards,
    );
  }
}
