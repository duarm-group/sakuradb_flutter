part of 'card_detail_cubit.dart';

enum CardDetailScreenStatus { initial, success, loading, error }

class CardDetailState extends Equatable {
  const CardDetailState(
      {this.status = CardDetailScreenStatus.initial, required this.card});

  @override
  List<Object> get props => [status, card];

  final CardDetailScreenStatus status;
  final Card card;

  CardDetailState copyWith({
    CardDetailScreenStatus? status,
    Card? card,
  }) {
    return CardDetailState(
      status: status ?? this.status,
      card: card ?? this.card,
    );
  }
}
