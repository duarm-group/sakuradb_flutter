import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../domain/entities/card.dart';
import '../../../domain/repositories/card_repository.dart';

part 'card_detail_state.dart';

class CardDetailCubit extends Cubit<CardDetailState> {
  CardDetailCubit({required this.cardRepository})
      : super(const CardDetailState(
          card: Card(
            id: 0,
            name: "",
            alignment: 0,
          ),
        ));
  final CardRepository cardRepository;

  void getCard(int id) async {
    emit(state.copyWith(status: CardDetailScreenStatus.loading));
    final response = await cardRepository.getCard(id);

    response.fold(
      (e) {
        emit(state.copyWith(status: CardDetailScreenStatus.error));
      },
      (card) {
        emit(state.copyWith(
          card: card,
          status: CardDetailScreenStatus.success,
        ));
      },
    );
  }
}
