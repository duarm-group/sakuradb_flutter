import 'package:get_it/get_it.dart';
import '../data/datasources/card_remote_data_source.dart';
import '../data/repositories/card_repository_impl.dart';
import '../domain/repositories/card_repository.dart';
import '../presentation/cubit/list/cards_cubit.dart';
import '../presentation/cubit/detail/card_detail_cubit.dart';

GetIt getIt = GetIt.instance;

class CardModule {
  static void inject() {
    getIt.registerSingleton<CardRemoteDataSource>(
      CardRemoteDataSourceImpl(getIt.get()),
    );

    getIt.registerSingleton<CardRepository>(
      CardRepositoryImpl(remoteDataSource: getIt.get()),
    );

    getIt.registerSingleton<CardsCubit>(
      CardsCubit(cardRepository: getIt.get()),
    );

    getIt.registerFactory<CardDetailCubit>(
      () => CardDetailCubit(cardRepository: getIt.get()),
    );
  }
}
