import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../../core/error/exceptions.dart';
import '../models/card_model.dart';

abstract class CardRemoteDataSource {
  Future<CardDataModel> getCard(int id);

  Future<CardsDataModel> getCards();
}

class CardRemoteDataSourceImpl implements CardRemoteDataSource {
  final http.Client client;

  CardRemoteDataSourceImpl(this.client);

  @override
  Future<CardDataModel> getCard(int id) async {
    final response = await client.get(
      Uri.parse("http://10.0.2.2:8000/api/v1/cards/$id"),
      headers: {
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 200) {
      return CardDataModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }

  @override
  Future<CardsDataModel> getCards() async {
    final response = await client.get(
      Uri.parse("http://10.0.2.2:8000/api/v1/cards/"),
      headers: {
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 200) {
      return CardsDataModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }
}
