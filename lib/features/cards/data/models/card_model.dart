import '../../domain/entities/card.dart';

class CardModel extends Card {
  CardModel(
      {required super.id,
      required super.name,
      required super.alignment,
      super.image});

  factory CardModel.fromJson(Map<String, dynamic> map) {
    return CardModel(
      id: map['id'],
      name: map['name'],
      alignment: map['alignment'],
      image: map['image'],
    );
  }
}

class CardDataModel {
  CardDataModel({required this.code, required this.msg, required this.data});

  final int code;
  final String msg;
  final Card data;

  factory CardDataModel.fromJson(Map<String, dynamic> map) {
    return CardDataModel(
      code: map['code'],
      msg: map['msg'],
      data: CardModel.fromJson(map['data']),
    );
  }
}

class CardsData {
  CardsData({required this.lists, required this.total});

  final List<Card> lists;
  final int total;

  factory CardsData.fromJson(Map<String, dynamic> map) {
    return CardsData(
      lists: List.from(map['lists']).map((e) => CardModel.fromJson(e)).toList(),
      total: map['total'],
    );
  }
}

class CardsDataModel {
  CardsDataModel({required this.code, required this.msg, required this.data});

  final int code;
  final String msg;
  final CardsData data;

  factory CardsDataModel.fromJson(Map<String, dynamic> map) {
    return CardsDataModel(
      code: map['code'],
      msg: map['msg'],
      data: CardsData.fromJson(map['data']),
    );
  }
}
