import 'dart:io';
import 'package:dartz/dartz.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failure.dart';
import '../../domain/entities/card.dart';
import '../datasources/card_remote_data_source.dart';
import '../../domain/repositories/card_repository.dart';

class CardRepositoryImpl implements CardRepository {
  CardRepositoryImpl({required this.remoteDataSource});

  final CardRemoteDataSource remoteDataSource;

  @override
  Future<Either<Failure, Card>> getCard(int id) async {
    try {
      final card = await remoteDataSource.getCard(id);
      return Right(card.data);
    } on ServerException {
      return Left(ServerFailure());
    } on SocketException {
      return Left(ServerFailure());
    } catch (e) {
      return Left(OtherFailure());
    }
  }

  @override
  Future<Either<Failure, List<Card>>> getCards() async {
    try {
      final cards = await remoteDataSource.getCards();
      return Right(cards.data.lists);
    } on ServerException {
      return Left(ServerFailure());
    } on SocketException {
      return Left(ServerFailure());
    } catch (e) {
      return Left(OtherFailure());
    }
  }
}
