import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import '../../../../core/theme/base_theme.dart';
import '../../../../core/theme/widgets/core_scaffold.dart';
import '../widgets/home_button_widget.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    final _ = context.read<BaseTheme>();
    return CoreScaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(title),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          // Padding(
          //   padding: const EdgeInsets.only(bottom: CoreSpacing.spacing2),
          //   child: Card(
          //     color: theme.pink,
          //     child: Padding(
          //       padding: EdgeInsets.symmetric(
          //         horizontal: CoreSpacing.spacing2,
          //         vertical: CoreSpacing.spacing2,
          //       ),
          //       child: Column(
          //         crossAxisAlignment: CrossAxisAlignment.start,
          //         children: [
          //           Text(
          //             "Welcome!",
          //             style: TextStyle(color: theme.gray900),
          //           ),
          //         ],
          //       ),
          //     ),
          //   ),
          // ),
          Expanded(
            child: GridView.count(
              crossAxisCount: 2,
              children: [
                HomeButton(
                  title: "Characters",
                  subtitle: "Every character",
                  image: 'assets/images/characters/sakura.jpg',
                  onTap: () => context.go('/characters'),
                ),
                HomeButton(
                  title: "Animes",
                  subtitle: "Every anime",
                  image: 'assets/images/animes/ccs_logo.png',
                  onTap: () => context.go('/animes'),
                ),
                HomeButton(
                  title: "Manga",
                  subtitle: "Every manga",
                  image: 'assets/images/volumes/CCS_Vol1.jpg',
                  onTap: () => context.go('/manga'),
                ),
                HomeButton(
                  title: "Costumes",
                  subtitle: "Every costume",
                  image: 'assets/images/costumes/Catch_You_Catch_Me_Dress.webp',
                  onTap: () => context.go('/costumes'),
                ),
                HomeButton(
                  title: "Cards",
                  subtitle: "Every card",
                  image: 'assets/images/cards/Repair.jpg',
                  onTap: () => context.go('/cards'),
                ),
                HomeButton(
                  title: "Video Games",
                  subtitle: "Every video game",
                  image: 'assets/images/videogames/Animetic-story-game.webp',
                  onTap: () => context.go('/video_games'),
                ),
                HomeButton(
                  title: "Writers",
                  subtitle: "Every writer",
                  image: 'assets/images/person/nanase.jpg',
                  onTap: () => context.go('/video_games'),
                ),
                HomeButton(
                  title: "Directors",
                  subtitle: "Every director",
                  image: 'assets/images/person/nanase.jpg',
                  onTap: () => context.go('/video_games'),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
