import 'package:flutter/material.dart';

class HomeButton extends StatelessWidget {
  const HomeButton(
      {super.key,
      required this.title,
      required this.subtitle,
      this.onTap,
      required this.image,
      this.clipBehavior = Clip.antiAlias});

  final String title;
  final String subtitle;
  final void Function()? onTap;
  final Clip? clipBehavior;
  final String image;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Card(
        clipBehavior: clipBehavior,
        child: Wrap(
          children: [
            ListTile(
              title: Text(
                title,
                style: const TextStyle(),
              ),
              subtitle: Text(
                subtitle,
                style: TextStyle(
                    overflow: TextOverflow.ellipsis,
                    color: Colors.black.withOpacity(0.6)),
              ),
            ),
            Image.asset(
              isAntiAlias: true,
              image,
            ),
          ],
        ),
      ),
    );
  }
}
