import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../../core/error/exceptions.dart';
import '../models/character_model.dart';

abstract class CharacterRemoteDataSource {
  Future<CharacterDataModel> getCharacter(int id);

  Future<CharactersModel> getCharacters();
}

class CharacterRemoteDataSourceImpl implements CharacterRemoteDataSource {
  final http.Client client;

  CharacterRemoteDataSourceImpl(this.client);

  @override
  Future<CharacterDataModel> getCharacter(int id) async {
    final response = await client.get(
      Uri.parse("http://10.0.2.2:8000/api/v1/characters/$id"),
      headers: {
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 200) {
      return CharacterDataModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }

  @override
  Future<CharactersModel> getCharacters() async {
    final response = await client.get(
      Uri.parse("http://10.0.2.2:8000/api/v1/characters/"),
      headers: {
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 200) {
      return CharactersModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }
}
