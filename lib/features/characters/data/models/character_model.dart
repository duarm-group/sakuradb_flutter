import 'package:sakuradb_app/features/characters/domain/entities/character.dart';

class CharacterModel extends Character {
  CharacterModel(
      {required super.id,
      required super.name,
      required super.nameTranslated,
      required super.sex,
      required super.image,
      super.about});

  factory CharacterModel.fromJson(Map<String, dynamic> map) {
    return CharacterModel(
      id: map['id'],
      name: map['name'],
      nameTranslated: map['name_translated'],
      sex: map['sex'],
      image: map['image'],
      about: map['about'],
    );
  }
}

class CharacterDataModel {
  CharacterDataModel(
      {required this.code, required this.msg, required this.data});

  final int code;
  final String msg;
  final Character data;

  factory CharacterDataModel.fromJson(Map<String, dynamic> map) {
    return CharacterDataModel(
      code: map['code'],
      msg: map['msg'],
      data: CharacterModel.fromJson(map['data']),
    );
  }
}

class CharactersData {
  CharactersData({required this.lists, required this.total});

  final List<Character> lists;
  final int total;

  factory CharactersData.fromJson(Map<String, dynamic> map) {
    return CharactersData(
      lists: List.from(map['lists'])
          .map((e) => CharacterModel.fromJson(e))
          .toList(),
      total: map['total'],
    );
  }
}

class CharactersModel {
  CharactersModel({required this.code, required this.msg, required this.data});

  final int code;
  final String msg;
  final CharactersData data;

  factory CharactersModel.fromJson(Map<String, dynamic> map) {
    return CharactersModel(
      code: map['code'],
      msg: map['msg'],
      data: CharactersData.fromJson(map['data']),
    );
  }
}
