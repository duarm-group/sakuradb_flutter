import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:sakuradb_app/core/error/exceptions.dart';
import 'package:sakuradb_app/core/error/failure.dart';
import 'package:sakuradb_app/features/characters/data/datasources/character_remote_data_source.dart';
import 'package:sakuradb_app/features/characters/domain/entities/character.dart';
import 'package:sakuradb_app/features/characters/domain/repositories/character_repository.dart';

class CharacterRepositoryImpl implements CharacterRepository {
  CharacterRepositoryImpl({required this.remoteDataSource});

  final CharacterRemoteDataSource remoteDataSource;

  @override
  Future<Either<Failure, Character>> getCharacter(int id) async {
    try {
      final character = await remoteDataSource.getCharacter(id);
      return Right(character.data);
    } on ServerException {
      return Left(ServerFailure());
    } on SocketException {
      return Left(ServerFailure());
    } catch (e) {
      return Left(OtherFailure());
    }
  }

  @override
  Future<Either<Failure, List<Character>>> getCharacters() async {
    try {
      final characters = await remoteDataSource.getCharacters();
      return Right(characters.data.lists);
    } on ServerException {
      return Left(ServerFailure());
    } on SocketException {
      return Left(ServerFailure());
    } catch (e) {
      return Left(OtherFailure());
    }
  }
}
