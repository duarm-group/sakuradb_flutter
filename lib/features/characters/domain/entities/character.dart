class Character {
  final int id;
  final String name;
  final String nameTranslated;
  final int sex;
  final String? about;

  final String? image;

  const Character({
    required this.id,
    required this.name,
    required this.nameTranslated,
    required this.sex,
    this.image,
    this.about,
  });

  @override
  String toString() {
    return "($id,$name,$nameTranslated,$sex,$image,$about)";
  }
}
