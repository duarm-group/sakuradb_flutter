part of 'character_detail_cubit.dart';

enum CharacterDetailScreenStatus { initial, success, loading, error }

class CharacterDetailState extends Equatable {
  const CharacterDetailState(
      {this.status = CharacterDetailScreenStatus.initial,
      required this.character});

  @override
  List<Object> get props => [status];

  final CharacterDetailScreenStatus status;
  final Character character;

  CharacterDetailState copyWith({
    CharacterDetailScreenStatus? status,
    Character? character,
  }) {
    return CharacterDetailState(
      status: status ?? this.status,
      character: character ?? this.character,
    );
  }
}
