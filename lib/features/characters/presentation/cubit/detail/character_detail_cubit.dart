import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../domain/entities/character.dart';
import '../../../domain/repositories/character_repository.dart';

part 'character_detail_state.dart';

class CharacterDetailCubit extends Cubit<CharacterDetailState> {
  CharacterDetailCubit({required this.characterRepository})
      : super(const CharacterDetailState(
          character: Character(
            id: 0,
            name: "",
            nameTranslated: "",
            sex: 1,
          ),
        ));
  final CharacterRepository characterRepository;

  void getCharacter(int id) async {
    emit(state.copyWith(status: CharacterDetailScreenStatus.loading));
    final response = await characterRepository.getCharacter(id);

    response.fold(
      (e) {
        emit(state.copyWith(status: CharacterDetailScreenStatus.error));
      },
      (character) {
        emit(state.copyWith(
          character: character,
          status: CharacterDetailScreenStatus.success,
        ));
      },
    );
  }
}
