import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../domain/entities/character.dart';
import '../../../domain/repositories/character_repository.dart';

part 'characters_state.dart';

class CharactersCubit extends Cubit<CharactersState> {
  CharactersCubit({required this.characterRepository})
      : super(const CharactersState());
  final CharacterRepository characterRepository;

  void getCharacters() async {
    emit(state.copyWith(status: CharactersScreenStatus.loading));
    final response = await characterRepository.getCharacters();
    response.fold(
      (e) {
        emit(state.copyWith(status: CharactersScreenStatus.error));
      },
      (characters) {
        emit(state.copyWith(
          characters: characters,
          status: CharactersScreenStatus.success,
        ));
      },
    );
  }
}
