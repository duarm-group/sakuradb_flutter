part of 'characters_cubit.dart';

enum CharactersScreenStatus { initial, success, loading, error }

class CharactersState extends Equatable {
  const CharactersState(
      {this.status = CharactersScreenStatus.initial,
      this.characters = const []});

  @override
  List<Object> get props => [status, characters];

  final CharactersScreenStatus status;
  final List<Character> characters;

  CharactersState copyWith({
    CharactersScreenStatus? status,
    List<Character>? characters,
  }) {
    return CharactersState(
      status: status ?? this.status,
      characters: characters ?? this.characters,
    );
  }
}
