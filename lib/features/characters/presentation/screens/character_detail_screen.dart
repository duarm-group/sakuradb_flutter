import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shimmer/shimmer.dart';

import '../../../../core/theme/base_theme.dart';
import '../../../../core/theme/widgets/core_scaffold.dart';
import '../cubit/detail/character_detail_cubit.dart';

class CharacterDetailScreen extends StatelessWidget {
  const CharacterDetailScreen({super.key, required this.id});

  final int id;

  @override
  Widget build(BuildContext context) {
    context.read<CharacterDetailCubit>().getCharacter(id);
    final theme = context.read<BaseTheme>();

    return CoreScaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(56),
        child: BlocBuilder<CharacterDetailCubit, CharacterDetailState>(
          builder: (context, state) {
            return AppBar(
              title: Text(state.character.nameTranslated),
            );
          },
        ),
      ),
      body: BlocBuilder<CharacterDetailCubit, CharacterDetailState>(
        builder: (context, state) {
          if (state.status == CharacterDetailScreenStatus.success) {
            return SingleChildScrollView(
              child: Column(children: [
                AspectRatio(
                  aspectRatio: 1.2,
                  child: ShaderMask(
                    shaderCallback: (rect) {
                      return const LinearGradient(
                        begin: FractionalOffset(0, 0.5),
                        end: FractionalOffset(0, 0.9),
                        colors: [Colors.black, Colors.transparent],
                      ).createShader(
                        Rect.fromLTRB(0, 0, rect.width, 350),
                      );
                    },
                    blendMode: BlendMode.dstIn,
                    child: Image.asset(
                      'assets/images/${state.character.image}',
                      isAntiAlias: true,
                      fit: BoxFit.fitWidth,
                      alignment: const FractionalOffset(0.0, 0.2),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: CoreSpacing.spacing3),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Flexible(
                            child: Text(
                              state.character.name,
                              style: const TextStyle(
                                  fontSize: 18,
                                  overflow: TextOverflow.ellipsis,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                          if (state.character.sex == 2)
                            Icon(
                              Icons.female,
                              color: theme.pink,
                            ),
                          if (state.character.sex == 1)
                            const Icon(
                              Icons.male,
                              color: Color(0xFF78a2d5),
                            ),
                        ],
                      ),
                      Text(
                        state.character.nameTranslated,
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w400),
                      ),
                      const SizedBox(
                        height: CoreSpacing.spacing2,
                      ),
                      if (state.character.about != null)
                        Text(
                          state.character.about!,
                          style: TextStyle(
                            fontSize: 14,
                            color: theme.gray700,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                    ],
                  ),
                ),
              ]),
            );
          }
          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: FractionallySizedBox(
                  widthFactor: 1,
                  alignment: Alignment.topCenter,
                  heightFactor: 0.3,
                  child: Shimmer.fromColors(
                    baseColor: theme.gray400.withOpacity(0.4),
                    highlightColor: theme.gray400.withOpacity(0.1),
                    child: Container(
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
              Shimmer.fromColors(
                baseColor: theme.gray400.withOpacity(0.4),
                highlightColor: theme.gray400.withOpacity(0.1),
                child: Container(
                  color: Colors.black,
                ),
              ),
              Shimmer.fromColors(
                baseColor: theme.gray400.withOpacity(0.4),
                highlightColor: theme.gray400.withOpacity(0.1),
                child: Container(
                  color: Colors.black,
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
