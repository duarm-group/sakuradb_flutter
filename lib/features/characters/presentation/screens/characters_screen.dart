import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:sakuradb_app/core/error/widgets/error_screen.dart';
import 'package:sakuradb_app/core/theme/base_theme.dart';
import 'package:shimmer/shimmer.dart';

import '../../../../core/theme/widgets/core_scaffold.dart';
import '../../../home/presentation/widgets/home_button_widget.dart';
import '../cubit/list/characters_cubit.dart';

class CharactersScreen extends StatelessWidget {
  const CharactersScreen({super.key});

  @override
  Widget build(BuildContext context) {
    context.read<CharactersCubit>().getCharacters();
    final theme = context.read<BaseTheme>();

    return CoreScaffold(
      appBar: AppBar(
        title: const Text("Characters"),
        actions: [
          InkWell(
            child: const Icon(
              Icons.filter_list,
              size: 30,
            ),
            onTap: () {},
          ),
          const SizedBox(width: CoreSpacing.spacing3),
        ],
      ),
      body: BlocBuilder<CharactersCubit, CharactersState>(
        builder: (context, state) {
          if (state.status == CharactersScreenStatus.loading) {
            return GridView.builder(
              itemCount: 5,
              itemBuilder: (_, index) => Shimmer.fromColors(
                baseColor: theme.gray400.withOpacity(0.4),
                highlightColor: theme.gray400.withOpacity(0.1),
                child: Container(
                  color: Colors.black,
                ),
              ),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 5.0,
                mainAxisSpacing: 5.0,
                mainAxisExtent: 300,
              ),
            );
          } else if (state.status == CharactersScreenStatus.error) {
            return const ErrorScreen(
                title: "Looks like a server error ocurred",
                subtitle: " hoeee~~");
          }
          return GridView.builder(
            itemCount: state.characters.length,
            itemBuilder: (_, index) => HomeButton(
              title: state.characters[index].nameTranslated,
              subtitle: state.characters[index].name,
              image: 'assets/images/${state.characters[index].image}',
              onTap: () => context.go(
                '/characters/detail',
                extra: state.characters[index].id,
              ),
            ),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 5.0,
              mainAxisSpacing: 5.0,
              mainAxisExtent: 300,
            ),
          );
        },
      ),
    );
  }
}
