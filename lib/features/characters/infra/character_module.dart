import 'package:get_it/get_it.dart';
import '../data/datasources/character_remote_data_source.dart';
import '../data/repositories/character_repository_impl.dart';
import '../domain/repositories/character_repository.dart';
import '../presentation/cubit/list/characters_cubit.dart';
import '../presentation/cubit/detail/character_detail_cubit.dart';

GetIt getIt = GetIt.instance;

class CharacterModule {
  static void inject() {
    getIt.registerSingleton<CharacterRemoteDataSource>(
      CharacterRemoteDataSourceImpl(getIt.get()),
    );

    getIt.registerSingleton<CharacterRepository>(
      CharacterRepositoryImpl(remoteDataSource: getIt.get()),
    );

    getIt.registerSingleton<CharactersCubit>(
      CharactersCubit(characterRepository: getIt.get()),
    );

    getIt.registerFactory<CharacterDetailCubit>(
      () => CharacterDetailCubit(characterRepository: getIt.get()),
    );
  }
}
