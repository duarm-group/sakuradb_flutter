import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';

import 'core/theme/theme_module.dart';
import 'core/network/http_module.dart';
import 'core/router/routes.dart';
import 'core/environment/environments_module.dart';
import 'core/theme/base_theme.dart';
import 'features/cards/infra/cards_module.dart';
import 'features/characters/infra/character_module.dart';
import 'features/animes/infra/anime_module.dart';

GetIt getIt = GetIt.instance;

void main() {
  HttpModule.inject();
  CharacterModule.inject();
  CardModule.inject();
  AnimeModule.inject();
  EnvironmentsModule.inject();
  ThemeModule.inject();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final BaseTheme theme = getIt.get();
    final router = AppRouter(getIt: getIt, initialRoute: '/');
    return MultiProvider(
      providers: [
        Provider<BaseTheme>.value(value: theme),
      ],
      child: MaterialApp.router(
        title: 'SakuraDB Browser',
        theme: theme.themeData(),
        routerConfig: router.routes(),
      ),
    );
  }
}
