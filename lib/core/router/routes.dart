import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:go_router/go_router.dart';

import '../../features/animes/presentation/cubits/detail/anime_detail_cubit.dart';
import '../../features/animes/presentation/screens/anime_detail_screen.dart';
import '../../features/animes/presentation/screens/animes_screen.dart';
import '../../features/cards/presentation/cubit/detail/card_detail_cubit.dart';
import '../../features/cards/presentation/cubit/list/cards_cubit.dart';
import '../../features/cards/presentation/screens/card_detail_screen.dart';
import '../../features/cards/presentation/screens/cards_screen.dart';
import '../../features/characters/presentation/cubit/list/characters_cubit.dart';
import '../../features/home/presentation/screens/home_screen.dart';
import '../../features/animes/presentation/cubits/list/animes_cubit.dart';
import '../../features/characters/presentation/cubit/detail/character_detail_cubit.dart';
import '../../features/characters/presentation/screens/character_detail_screen.dart';
import '../../features/characters/presentation/screens/characters_screen.dart';

class AppRouter {
  final GetIt getIt;
  final String initialRoute;

  const AppRouter({required this.getIt, required this.initialRoute});

  GoRouter routes() => GoRouter(
        initialLocation: initialRoute,
        routes: [
          GoRoute(
            path: '/',
            builder: (context, state) =>
                const HomeScreen(title: "Sakura DB Browser"),
            routes: [
              GoRoute(
                path: 'characters',
                builder: (context, state) =>
                    BlocProvider<CharactersCubit>.value(
                  value: getIt.get(),
                  child: const CharactersScreen(),
                ),
                routes: [
                  GoRoute(
                    path: 'detail',
                    builder: (context, state) =>
                        BlocProvider<CharacterDetailCubit>.value(
                      value: getIt.get(),
                      child: CharacterDetailScreen(id: state.extra as int),
                    ),
                  ),
                ],
              ),
              GoRoute(
                path: 'animes',
                builder: (context, state) => BlocProvider<AnimesCubit>.value(
                  value: getIt.get(),
                  child: const AnimesScreen(),
                ),
                routes: [
                  GoRoute(
                    path: 'detail',
                    builder: (context, state) =>
                        BlocProvider<AnimeDetailCubit>.value(
                      value: getIt.get(),
                      child: AnimeDetailScreen(id: state.extra as int),
                    ),
                  ),
                ],
              ),
              GoRoute(
                path: 'episodes',
                builder: (context, state) =>
                    BlocProvider<CharactersCubit>.value(
                  value: getIt.get(),
                  child: const CharactersScreen(),
                ),
                routes: [
                  GoRoute(
                    path: 'detail',
                    builder: (context, state) =>
                        BlocProvider<CharacterDetailCubit>.value(
                      value: getIt.get(),
                      child: CharacterDetailScreen(id: state.extra as int),
                    ),
                  ),
                ],
              ),
              GoRoute(
                path: 'cards',
                builder: (context, state) => BlocProvider<CardsCubit>.value(
                  value: getIt.get(),
                  child: const CardsScreen(),
                ),
                routes: [
                  GoRoute(
                    path: 'detail',
                    builder: (context, state) =>
                        BlocProvider<CardDetailCubit>.value(
                      value: getIt.get(),
                      child: CardDetailScreen(id: state.extra as int),
                    ),
                  ),
                ],
              ),
              GoRoute(
                path: 'video_games',
                builder: (context, state) =>
                    BlocProvider<CharactersCubit>.value(
                  value: getIt.get(),
                  child: const CharactersScreen(),
                ),
                routes: [
                  GoRoute(
                    path: 'detail',
                    builder: (context, state) =>
                        BlocProvider<CharacterDetailCubit>.value(
                      value: getIt.get(),
                      child: CharacterDetailScreen(id: state.extra as int),
                    ),
                  ),
                ],
              ),
              GoRoute(
                path: 'manga',
                builder: (context, state) =>
                    BlocProvider<CharactersCubit>.value(
                  value: getIt.get(),
                  child: const CharactersScreen(),
                ),
                routes: [
                  GoRoute(
                    path: 'detail',
                    builder: (context, state) =>
                        BlocProvider<CharacterDetailCubit>.value(
                      value: getIt.get(),
                      child: CharacterDetailScreen(id: state.extra as int),
                    ),
                  ),
                ],
              ),
              GoRoute(
                path: 'costumes',
                builder: (context, state) =>
                    BlocProvider<CharactersCubit>.value(
                  value: getIt.get(),
                  child: const CharactersScreen(),
                ),
                routes: [
                  GoRoute(
                    path: 'detail',
                    builder: (context, state) =>
                        BlocProvider<CharacterDetailCubit>.value(
                      value: getIt.get(),
                      child: CharacterDetailScreen(id: state.extra as int),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      );
}
