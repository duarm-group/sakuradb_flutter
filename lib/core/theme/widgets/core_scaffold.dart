import 'package:flutter/material.dart';

class CoreScaffold extends StatelessWidget {
  const CoreScaffold({super.key, required this.body, this.appBar});

  final Widget body;
  final PreferredSizeWidget? appBar;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar,
      floatingActionButton: null,
      body: SafeArea(
        child: body,
      ),
    );
  }
}
