import 'package:flutter/material.dart';

class CoreSpacing {
  static const double spacing1 = 4;
  static const double spacing2 = 8;
  static const double spacing3 = 16;
  static const double spacing4 = 24;
  static const double spacing5 = 32;
  static const double spacing6 = 48;
  static const double spacing7 = 64;
  static const double borderRadius = 8;
}

class BaseTheme {
  Color get background => const Color(0xFFFFFFFF);

  Color get backgroundDark => const Color(0xFF595959);

  Color get primary => const Color(0xFFFFFFFF);

  Color get primaryDark => const Color(0xFFc80606);

  Color get secondary => const Color(0xFF1B7BC2);

  Color get secondaryDark => const Color(0xFF3F3F3F);

  Color get highlight => const Color(0xFF80ED99);

  Color get highlightDark => const Color(0xFF57CC99);

  Color get success => const Color(0xFF18D82E);

  Color get successSecondary => const Color(0xFFE5FFE2);

  Color get error => const Color(0xFFFE2A25);

  Color get pink => const Color(0xFFf9d1d1);

  Color get warning => const Color(0xFFFFF4CC);

  Color get info => const Color(0xFFA8DCD9);

  Color get white => const Color(0xFFFFFFFF);

  Color get black => const Color(0xFF000000);

  Color get gray900 => const Color(0xFF141414);

  Color get gray800 => const Color(0xFF1F1F1F);

  Color get gray700 => const Color(0xFF333333);

  Color get gray600 => const Color(0xFF545454);

  Color get gray500 => const Color(0xFF757575);

  Color get gray400 => const Color(0xFFAFAFAF);

  Color get gray200 => const Color(0xFFE2E2E2);

  Color get gray100 => const Color(0xFFEEEEEE);

  ThemeData themeData() {
    return ThemeData(
      colorScheme: ColorScheme(
          background: background,
          primary: primary,
          brightness: Brightness.light,
          secondary: secondary,
          onPrimary: primary,
          surface: background,
          onBackground: black,
          onSecondary: white,
          onSurface: black,
          onError: white,
          error: error),
      canvasColor: background,
      scaffoldBackgroundColor: background,
      buttonTheme: const ButtonThemeData(
        materialTapTargetSize: MaterialTapTargetSize.padded,
      ),
      switchTheme: SwitchThemeData(
        overlayColor: MaterialStateProperty.resolveWith(
          (states) {
            if (states.contains(MaterialState.selected)) {
              return primary;
            }
            return gray600;
          },
        ),
        trackColor: MaterialStateProperty.resolveWith(
          (states) {
            if (states.contains(MaterialState.selected)) {
              return secondary;
            }
            return gray600;
          },
        ),
        thumbColor: MaterialStateProperty.resolveWith(
          (states) {
            return Colors.white;
          },
        ),
        trackOutlineColor: MaterialStateProperty.resolveWith(
          (states) {
            return Colors.transparent;
          },
        ),
      ),
      cardTheme: CardTheme(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.zero,
          side: BorderSide(
            color: gray200,
          ),
        ),
      ),
      menuTheme: MenuThemeData(
        style: MenuStyle(
          padding: MaterialStateProperty.resolveWith(
            (states) {
              return const EdgeInsets.symmetric(
                horizontal: CoreSpacing.spacing2,
                vertical: CoreSpacing.spacing2,
              );
            },
          ),
          elevation: MaterialStateProperty.resolveWith(
            (states) {
              return 1;
            },
          ),
          backgroundColor: MaterialStateProperty.resolveWith(
            (states) {
              if (states.contains(MaterialState.selected)) {
                return white;
              } else {
                return white;
              }
            },
          ),
        ),
      ),
      checkboxTheme: CheckboxThemeData(
        fillColor: MaterialStateProperty.resolveWith((states) {
          if (states.contains(MaterialState.selected)) {
            return secondary;
          } else {
            return white;
          }
        }),
        checkColor: MaterialStateProperty.resolveWith((states) {
          if (states.contains(MaterialState.selected)) {
            return secondary;
          }
          return null;
        }),
      ),
      bottomAppBarTheme: BottomAppBarTheme(
        color: background,
        surfaceTintColor: const Color(0x00FFFFFF),
      ),
      appBarTheme: AppBarTheme(color: background),
      bottomNavigationBarTheme: BottomNavigationBarThemeData(
        elevation: 16,
        showUnselectedLabels: true,
        unselectedItemColor: secondaryDark,
        unselectedIconTheme: IconThemeData(
          color: secondaryDark,
        ),
        selectedItemColor: primary,
        selectedIconTheme: IconThemeData(
          color: primary,
        ),
      ),
    );
  }
}
