import 'package:get_it/get_it.dart';
import 'package:sakuradb_app/core/theme/base_theme.dart';

GetIt getIt = GetIt.instance;

class ThemeModule {
  static void inject() {
    getIt.registerSingleton<BaseTheme>(BaseTheme());
  }
}
