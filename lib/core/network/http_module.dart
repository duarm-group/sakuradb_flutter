import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

GetIt getIt = GetIt.instance;

class HttpModule {
  static void inject() {
    getIt.registerSingleton<http.Client>(http.Client());
  }
}
