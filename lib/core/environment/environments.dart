class Environments {
  String get baseUrl => 'http://10.0.2.2:8000';
  int get timeout => const Duration(seconds: 10).inMilliseconds;
}