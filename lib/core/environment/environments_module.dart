import 'package:get_it/get_it.dart';
import 'environments.dart';

GetIt getIt = GetIt.instance;

class EnvironmentsModule {
  static void inject() {
    getIt.registerSingleton<Environments>(Environments());
  }
}
