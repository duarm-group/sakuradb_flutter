import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../theme/base_theme.dart';

class ErrorScreen extends StatelessWidget {
  const ErrorScreen({super.key, required this.title, required this.subtitle});

  final String title;
  final String subtitle;

  @override
  Widget build(BuildContext context) {
    final theme = context.read<BaseTheme>();
    return Center(
      heightFactor: 0.8,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: CoreSpacing.spacing3),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.error_outline,
              size: 80,
              color: theme.error,
            ),
            const SizedBox(height: CoreSpacing.spacing2),
            Text(
              title,
              style: TextStyle(
                  fontSize: 24,
                  color: theme.gray600,
                  fontWeight: FontWeight.w600),
            ),
            Text(
              subtitle,
              style: TextStyle(
                  fontSize: 16,
                  color: theme.gray600,
                  fontWeight: FontWeight.w400),
            ),
          ],
        ),
      ),
    );
  }
}
